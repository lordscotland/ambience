ARCHS = arm64

include theos/makefiles/common.mk

APPLICATION_NAME = Ambience
Ambience_FILES = $(wildcard *.m)
Ambience_FRAMEWORKS = UIKit AVFoundation

include $(THEOS_MAKE_PATH)/application.mk
