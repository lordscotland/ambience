struct audio_item {
  NSString* displayName;
  NSString* sectionName;
  NSString* fileName;
};

@protocol ListControllerDelegate
-(void)tableView:(UITableView*)view didSelectItemAtIndex:(NSUInteger)index forRowAtIndexPath:(NSIndexPath*)ipath;
@end

@interface ListController : UITableViewController {
  struct audio_item* items;
  NSUInteger itemCount,selectedIndex;
  id<ListControllerDelegate> delegate;
  UITableView* delegateTableView;
  NSIndexPath* delegateIndexPath;
}
-(id)initWithItems:(struct audio_item*)_items count:(NSUInteger)_itemCount selectedIndex:(NSUInteger)_selectedIndex delegate:(id<ListControllerDelegate>)_delegate tableView:(UITableView*)_delegateTableView indexPath:(NSIndexPath*)_delegateIndexPath;
@end
