#import <AVFoundation/AVFoundation.h>
#include "ListController.h"
#define NSECTIONS 3

@interface AppDelegate : NSObject <UIApplicationDelegate,UITableViewDataSource,UITableViewDelegate,AVAudioPlayerDelegate,ListControllerDelegate> {
  NSURL* mediaURL;
  UITableView* mainTableView;
  AVAudioPlayer* player[NSECTIONS];
  struct audio_item* list[NSECTIONS];
  NSUInteger itemCount[NSECTIONS],selectedIndex[NSECTIONS];
  float volume[NSECTIONS];
  BOOL isPlaying;
}
@property(retain,nonatomic) UIWindow* window;
@end
