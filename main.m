int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  int retval=UIApplicationMain(argc,argv,nil,@"AppDelegate");
  [pool drain];
  return retval;
}
