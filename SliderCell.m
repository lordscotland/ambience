#include "SliderCell.h"

@interface UIImage (Private)
+(UIImage*)imageNamed:(NSString*)name inBundle:(NSBundle*)bundle;
@end

@implementation SliderCell
@synthesize slider;
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier {
  if((self=[super initWithStyle:style reuseIdentifier:identifier])){
    slider=[[UISlider alloc] init];
    UIView* container=self.contentView;
    CGSize csize=container.bounds.size;
    CGFloat h=slider.frame.size.height;
    slider.frame=CGRectMake(15,(csize.height-h)/2,csize.width-30,h);
    slider.autoresizingMask=UIViewAutoresizingFlexibleWidth
     |UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    NSBundle* bundle=[NSBundle bundleWithPath:@"/System/Library/PrivateFrameworks/MediaPlayerUI.framework"];
    slider.minimumValueImage=[UIImage imageNamed:@"volume-minimum-value-image" inBundle:bundle];
    slider.maximumValueImage=[UIImage imageNamed:@"volume-maximum-value-image" inBundle:bundle];
    [container addSubview:slider];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
  }
  return self;
}
-(void)dealloc {
  [slider release];
  [super dealloc];
}
@end
