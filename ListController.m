#include "ListController.h"

@implementation ListController
-(id)initWithItems:(struct audio_item*)_items count:(NSUInteger)_itemCount selectedIndex:(NSUInteger)_selectedIndex delegate:(id<ListControllerDelegate>)_delegate tableView:(UITableView*)_delegateTableView indexPath:(NSIndexPath*)_delegateIndexPath {
  if((self=[super initWithStyle:UITableViewStyleGrouped])){
    items=_items;
    itemCount=_itemCount;
    selectedIndex=_selectedIndex;
    delegate=_delegate;
    delegateTableView=[_delegateTableView retain];
    delegateIndexPath=[_delegateIndexPath retain];
  }
  return self;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)view {
  return 2;
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  return (section==1)?itemCount:1;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  UITableViewCell* cell=[view dequeueReusableCellWithIdentifier:@"NameCell"];
  if(!cell){
    cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
     reuseIdentifier:@"NameCell"] autorelease];
    cell.detailTextLabel.textColor=[UIColor lightGrayColor];
  }
  NSUInteger index=ipath.row+ipath.section;
  cell.textLabel.text=index?items[index-1].displayName:@"-None-";
  cell.detailTextLabel.text=index?items[index-1].sectionName:nil;
  cell.accessoryType=(index==selectedIndex)?
   UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
  return cell;
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [delegate tableView:delegateTableView
   didSelectItemAtIndex:ipath.row+ipath.section
   forRowAtIndexPath:delegateIndexPath];
  [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidLoad {
  CFRunLoopPerformBlock(CFRunLoopGetMain(),kCFRunLoopDefaultMode,^{
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath
     indexPathForRow:selectedIndex?selectedIndex-1:0 inSection:selectedIndex?1:0]
     atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
  });
}
-(void)dealloc {
  [delegateTableView release];
  [delegateIndexPath release];
  [super dealloc];
}
@end
