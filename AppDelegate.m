#include "AppDelegate.h"
#include "SliderCell.h"

NSString* const $_sectionKey[]={@"Music",@"Sound",@"Effect"};
NSString* const $_volumeKey[]={@"vMusic",@"vSound",@"vEffect"};

static NSURL* $_subURL(NSURL* dirURL,NSUInteger z) {
  switch(z){
    case 0:return dirURL;
    case 1:return [dirURL URLByAppendingPathComponent:@"nature"];
    case 2:return [dirURL URLByAppendingPathComponent:@"effect"];
  }
  return nil;
}
static void $_setPlaying(UITableViewCell* cell,BOOL isPlaying) {
  UILabel* label=cell.textLabel;
  label.text=isPlaying?@"Stop":@"Play";
  label.textColor=isPlaying?[UIColor redColor]:[UIColor darkGrayColor];
}

@implementation AppDelegate
@synthesize window;
-(BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)options {
  NSFileManager* manager=[NSFileManager defaultManager];
  NSBundle* bundle=[NSBundle mainBundle];
  mediaURL=[[[manager URLForDirectory:NSLibraryDirectory
   inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:NULL]
   URLByAppendingPathComponent:bundle.bundleIdentifier] retain];
  NSURL* cacheURL=[mediaURL URLByAppendingPathComponent:@"index.plist"];
  NSDictionary* cache=[NSDictionary dictionaryWithContentsOfURL:cacheURL];
  NSUInteger z;
  if(!cache){
    NSMutableArray* specs[NSECTIONS]={0};
    NSCharacterSet* nondigits=[[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    for (NSURL* dirURL in [manager enumeratorAtURL:mediaURL
     includingPropertiesForKeys:[NSArray arrayWithObject:NSURLIsDirectoryKey]
     options:NSDirectoryEnumerationSkipsSubdirectoryDescendants errorHandler:NULL]){
      NSNumber* isDirectory;
      if(![dirURL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL]
       || !isDirectory.boolValue){continue;}
      NSString* sectionName=dirURL.lastPathComponent;
      for (z=0;z<NSECTIONS;z++){
        for (NSURL* URL in [manager enumeratorAtURL:$_subURL(dirURL,z)
         includingPropertiesForKeys:nil
         options:NSDirectoryEnumerationSkipsSubdirectoryDescendants errorHandler:NULL]){
          NSNumber* isFile;
          if(![URL.URLByResolvingSymlinksInPath
           getResourceValue:&isFile forKey:NSURLIsRegularFileKey error:NULL]
           || !isFile.boolValue){continue;}
          NSString* fileName=URL.lastPathComponent;
          NSMutableString* spec=fileName.stringByDeletingPathExtension.mutableCopy;
          NSUInteger pos=[spec rangeOfCharacterFromSet:nondigits].location;
          if(pos!=NSNotFound && [spec characterAtIndex:pos]=='-'){
            [spec deleteCharactersInRange:NSMakeRange(0,pos+1)];
          }
          pos=[spec rangeOfString:@"." options:NSBackwardsSearch].location;
          if(pos!=NSNotFound && [nondigits characterIsMember:[spec characterAtIndex:pos+1]]){
            [spec replaceCharactersInRange:NSMakeRange(pos,1) withString:@" ("];
            [spec appendString:@")"];
          }
          if([spec hasPrefix:@"+"]){
            [spec replaceCharactersInRange:NSMakeRange(0,1) withString:@"* "];
          }
          [spec replaceOccurrencesOfString:@"^" withString:@"'"
           options:0 range:NSMakeRange(0,spec.length)];
          [spec replaceOccurrencesOfString:@"+" withString:@"/"
           options:0 range:NSMakeRange(0,spec.length)];
          [spec replaceOccurrencesOfString:@"," withString:@", "
           options:0 range:NSMakeRange(0,spec.length)];
          [spec replaceOccurrencesOfString:@"_" withString:@" "
           options:0 range:NSMakeRange(0,spec.length)];
          [spec appendFormat:@"\0%@/%@",sectionName,fileName];
          if(!specs[z]){specs[z]=[NSMutableArray array];}
          [specs[z] addObject:spec];
          [spec release];
        }
      }
    }
    cache=[NSMutableDictionary dictionary];
    for (z=0;z<NSECTIONS;z++){
      if(specs[z]){
        [specs[z] sortUsingSelector:@selector(localizedStandardCompare:)];
        [(NSMutableDictionary*)cache setObject:specs[z] forKey:$_sectionKey[z]];
      }
    }
    if(cache.count){
      NSOutputStream* stream=[NSOutputStream outputStreamWithURL:cacheURL append:NO];
      [stream open];
      [NSPropertyListSerialization writePropertyList:cache
       toStream:stream format:NSPropertyListBinaryFormat_v1_0 options:0 error:NULL];
      [stream close];
    }
  }
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  for (z=0;z<NSECTIONS;z++){
    NSString* selectedSpec=[defaults stringForKey:$_sectionKey[z]];
    NSArray* specs=[cache objectForKey:$_sectionKey[z]];
    if((itemCount[z]=specs.count)){
      list[z]=malloc(sizeof(struct audio_item)*itemCount[z]);
      NSUInteger i=0;
      for (NSString* spec in specs){
        NSUInteger pos=[spec rangeOfString:@"\0"].location;
        list[z][i].displayName=[[spec substringToIndex:pos] retain];
        pos=[spec=[spec substringFromIndex:pos+1] rangeOfString:@"/"].location;
        list[z][i].sectionName=[[spec substringToIndex:pos] retain];
        list[z][i].fileName=[[spec substringFromIndex:pos+1] retain];
        if([spec isEqualToString:selectedSpec]){selectedIndex[z]=i+1;}
        i++;
      }
    }
    volume[z]=[defaults floatForKey:$_volumeKey[z]];
  }
  [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
   withOptions:AVAudioSessionCategoryOptionMixWithOthers error:NULL];
  window=[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  mainTableView=[[UITableView alloc]
   initWithFrame:CGRectMake(0,0,0,0) style:UITableViewStyleGrouped];
  mainTableView.dataSource=self;
  mainTableView.delegate=self;
  UIViewController* controller=[[UIViewController alloc] init];
  controller.title=[bundle.infoDictionary objectForKey:(id)kCFBundleNameKey];
  controller.view=mainTableView;
  [window.rootViewController=[[UINavigationController alloc]
   initWithRootViewController:controller] release]; [controller release];
  [window makeKeyAndVisible];
  return YES;
}
-(void)togglePlayback {
  NSUInteger z;
  if(isPlaying){
    isPlaying=NO;
    for (z=0;z<NSECTIONS;z++){
      [player[z] stop];
      player[z].currentTime=0;
    }
  }
  else {
    for (z=0;z<NSECTIONS;z++){
      if(!player[z] && selectedIndex[z]){
        struct audio_item item=list[z][selectedIndex[z]-1];
        NSError* error=nil;
        if((player[z]=[[AVAudioPlayer alloc] initWithContentsOfURL:
         [$_subURL([mediaURL URLByAppendingPathComponent:item.sectionName],z)
         URLByAppendingPathComponent:item.fileName] error:&error])){
          player[z].volume=volume[z];
          player[z].delegate=self;
          if([item.displayName characterAtIndex:0]!='*'){
            player[z].numberOfLoops=-1;
          }
        }
        else {
          UIAlertView* alert=[[UIAlertView alloc]
           initWithTitle:[NSString stringWithFormat:@"Error %ld",(long)error.code]
           message:error.localizedDescription delegate:nil
           cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
          [alert show];
          [alert release];
        }
      }
      if(player[z]){
        isPlaying=YES;
        [player[z] prepareToPlay];
        [player[z] play];
      }
    }
    if(!isPlaying){return;}
  }
  [[AVAudioSession sharedInstance] setActive:isPlaying error:NULL];
  UITableViewCell* cell=[mainTableView cellForRowAtIndexPath:
   [NSIndexPath indexPathForRow:0 inSection:NSECTIONS]];
  if(cell){$_setPlaying(cell,isPlaying);}
}
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer*)_player successfully:(BOOL)success {
  NSUInteger z;
  for (z=0;z<NSECTIONS;z++){
    if(player[z].playing){return;}
  }
  [self togglePlayback];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)view {
  return NSECTIONS+1;
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)z {
  return (z==NSECTIONS)?1:2;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  NSUInteger z=ipath.section;
  if(z==NSECTIONS){
    UITableViewCell* cell=[view dequeueReusableCellWithIdentifier:@"PlayButton"];
    if(!cell){
      cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
       reuseIdentifier:@"PlayButton"] autorelease];
      cell.textLabel.textAlignment=NSTextAlignmentCenter;
    }
    $_setPlaying(cell,isPlaying);
    return cell;
  }
  if(ipath.row==1){
    SliderCell* cell=(id)[view dequeueReusableCellWithIdentifier:@"SliderCell"];
    if(!cell){
      cell=[[[SliderCell alloc] initWithStyle:UITableViewCellStyleDefault
       reuseIdentifier:@"SliderCell"] autorelease];
      [cell.slider addTarget:self action:@selector(sliderDidChange:)
       forControlEvents:UIControlEventValueChanged];
    }
    UISlider* slider=cell.slider;
    slider.tag=z;
    slider.enabled=selectedIndex[z]?YES:NO;
    slider.value=selectedIndex[z]?volume[z]:0;
    return cell;
  }
  UITableViewCell* cell=[view dequeueReusableCellWithIdentifier:@"NameCell"];
  if(!cell){
    cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
     reuseIdentifier:@"NameCell"] autorelease];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
  }
  cell.textLabel.text=$_sectionKey[z];
  cell.detailTextLabel.text=selectedIndex[z]?
   list[z][selectedIndex[z]-1].displayName:@"-None-";
  return cell;
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [view deselectRowAtIndexPath:ipath animated:YES];
  NSUInteger z=ipath.section;
  if(z==NSECTIONS){
    [self togglePlayback];
    return;
  }
  ListController* controller=[[ListController alloc]
   initWithItems:list[z] count:itemCount[z] selectedIndex:selectedIndex[z]
   delegate:self tableView:view indexPath:ipath];
  controller.title=$_sectionKey[z];
  [(UINavigationController*)window.rootViewController
   pushViewController:controller animated:YES];
  [controller release];
}
-(void)tableView:(UITableView*)view didSelectItemAtIndex:(NSUInteger)index forRowAtIndexPath:(NSIndexPath*)ipath {
  NSUInteger z=ipath.section;
  if(selectedIndex[z]!=index){
    if(isPlaying){[self togglePlayback];}
    [player[z] release];
    player[z]=nil;
  }
  UITableViewCell* cell=[view cellForRowAtIndexPath:ipath];
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  if((selectedIndex[z]=index)){
    struct audio_item item=list[z][index-1];
    [defaults setObject:[item.sectionName
     stringByAppendingFormat:@"/%@",item.fileName] forKey:$_sectionKey[z]];
    if(cell){cell.detailTextLabel.text=item.displayName;}
  }
  else {
    [defaults removeObjectForKey:$_sectionKey[z]];
    if(cell){cell.detailTextLabel.text=@"-None-";}
  }
  if((cell=[view cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:z]])){
    UISlider* slider=[(SliderCell*)cell slider];
    slider.enabled=index?YES:NO;
    slider.value=index?volume[z]:0;
  }
}
-(void)sliderDidChange:(UISlider*)slider {
  NSUInteger z=slider.tag;
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  if((volume[z]=slider.value)){[defaults setFloat:volume[z] forKey:$_volumeKey[z]];}
  else {[defaults removeObjectForKey:$_volumeKey[z]];}
  player[z].volume=volume[z];
}
-(void)dealloc {
  [mediaURL release];
  NSUInteger z;
  for (z=0;z<NSECTIONS;z++){
    [player[z] release];
    NSUInteger count=itemCount[z],i;
    if(count){
      for (i=0;i<count;i++){
        [list[z][i].displayName release];
        [list[z][i].sectionName release];
        [list[z][i].fileName release];
      }
      free(list[z]);
    }
  }
  [window release];
  [mainTableView release];
  [super dealloc];
}
@end
